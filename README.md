# PackageName

### What is this package for? ###

* This package contains shaders for sprites.
* Unity minimum version: 2018.3

### What can I do with it? ###
* You can use
	* **Script**: summary

### How do I get set up? ###
* Using the **Package Registry Server**:
	* Open the **manifest.json** file inside your Unity project's **Packages** folder;
	* Add this line before *"dependencies"* attribute:
		* ```"scopedRegistries": [ { "name": "Action Code", "url": "http://34.83.179.179:4873/", "scopes": [ "com.actioncode" ] } ],```
	* The package **ActionCode-SpriteShaders** will be available for you to install using the **Package Manager** windows.
	
* By **Git URL** (you'll need a **Git client** installed on your machine):
	* Add this line inside *"dependencies"* attribute: 
		* ```"com.actioncode.sprite-shaders":"https://bitbucket.org/nostgameteam/spriteshaders.git"```

### Who do I talk to? ###

* Repo owner and admin: **Hyago Oliveira** (hyagogow@gmail.com)